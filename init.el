;; Load-path tweaks.
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))

;;
;; Set up ELPA per https://github.com/MassimoLauria/dotemacs/
;;
(require 'package)

(setq package-archives
  '(
     ("gnu"          . "http://elpa.gnu.org/packages/")
     ("elpa"         . "http://tromey.com/elpa/")
     ("melpa-stable" . "http://stable.melpa.org/packages/")
     ("melpa"        . "http://melpa.org/packages/")
     ;; ("marmalade"    . "http://marmalade-repo.org/packages/")
   )) ;; end of package list

;; Initialiaze packages
(package-initialize)

;;------------------------------------------------------------------------------
;; On-demand installation of packages
;;------------------------------------------------------------------------------

(defun require-package (package &optional min-version no-refresh)
  "Ask elpa to install given PACKAGE."
  (if (package-installed-p package min-version)
      t
    (if (or (assoc package package-archive-contents) no-refresh)
        (package-install package)
      (progn
        (package-refresh-contents)
        (require-package package min-version t)))))

;;------------------------------------------------------------------------------
;; Install missing packages
;;------------------------------------------------------------------------------
(require-package 'auctex)
(require-package 'magit)
(require-package 'col-highlight)
(require-package 'crosshairs)
(require-package 'fill-column-indicator)
(require-package 'haskell-mode)
(require-package 'hl-line+)
(require-package 'psvn)
(require-package 'vline)

;; Unix PATH tweaks.
(if (not (eq system-type 'windows-nt))
  (setenv "PATH" (concat "/usr/local/bin" ":"
                         "/opt/local/bin" ":"
                         "/usr/texbin" ":" (getenv "PATH")))
)

;; Haskell mode.
(eval-after-load "haskell-mode"
 '(progn
   (define-key haskell-mode-map (kbd "C-x C-d") nil)
   (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
   (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-file)
   (define-key haskell-mode-map (kbd "C-c C-b") 'haskell-interactive-switch)
   (define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
   (define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)
   (define-key haskell-mode-map (kbd "C-c M-.") nil)
   (define-key haskell-mode-map (kbd "C-c C-d") nil)))
(setq haskell-process-show-debug-tips nil)
;(setq haskell-process-type 'ghci)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-decl-scan)
;(add-hook 'haskell-mode-hook
;	  (lambda () (interactive)
;	    (fci-mode 1)
;	    (local-set-key (kbd "M-n") 'haskell-ds-forward-decl)
;	    (local-set-key (kbd "M-p") 'haskell-ds-backward-decl)))

;; Proof General.
(condition-case nil
  (load "ProofGeneral/generic/proof-site")
  (error (message "Failed to load Proof General")))

;; Always show line and column numbers in the modeline.
(line-number-mode 1)
(column-number-mode 1)

;; Always highlight matching parens.
(show-paren-mode 1)

;; Crosshairs mode may be toggled with C-=.
(global-set-key (kbd "C-=") 'crosshairs-mode)

;; Inhibit splash screen.
(setq inhibit-splash-screen t)

;; Winner mode (can move through window configuration history with C-c left
;; and C-c right)
(when (fboundp 'winner-mode)
      (winner-mode 1))

;; So we can talk to Skim.
(server-start)

;; Move the fill column over.
(setq-default fill-column 77)

;; Tweak the fill column indicator color.
(setq fci-rule-color "#770000")

;; Turn off tool bar.
(tool-bar-mode -1)

;; Turn off widget images, use raised buttons.
(setq widget-image-enable nil)
(setq custom-raised-buttons t)

;; Fix meta/super keys for Mac.
(setq ns-alternate-modifier (quote super))
(setq ns-command-modifier (quote meta))

;; Set up Skim when on a Mac.
(when (eq system-type 'darwin)
 (add-hook 'TeX-mode-hook 'TeX-source-correlate-mode)
 (setq TeX-view-program-list
   (quote (("Skim" "/Applications/Skim.app/Contents/SharedSupport/displayline -g %n %o %b")
           ("Preview" "open -a Preview.app %o"))))
 (setq TeX-view-program-selection
   (quote ((output-dvi "open")
           (output-pdf "Skim")
           (output-html "open"))))
 )

;; Set up SumatraPDF when on Windows.
(when (eq system-type 'windows-nt)
 (add-hook 'TeX-mode-hook 'TeX-source-correlate-mode)
 (setq TeX-view-program-list
       (quote (("SumatraPDF"
		"\"C:/Program Files (x86)/SumatraPDF/SumatraPDF.exe\" -reuse-instance -forward-search %b %n %o"))))
 (setq TeX-view-program-selection
   (quote ((output-dvi style-pstricks "dvips and start")
           (output-dvi "Yap")
	   (output-pdf "SumatraPDF")
           (output-html "start")))
 )
)

;; Set default face to a dark bg.
(set-face-attribute 'default nil :background "Black")
(set-face-attribute 'default nil :foreground "White")

;; Set default font. (From www.emacswiki.org/SetFonts)
(require 'cl)
(defun font-candidate (&rest fonts)
   "Return existing font which first match."
   (find-if (lambda (f) (find-font (font-spec :name f))) fonts))

(set-face-attribute 'default nil :font
   (font-candidate '"Monaco-12:weight=normal"
                    "Consolas-12:weight=normal"
                    "Courier New-12:weight=normal")
)

;; Set column and row highlight faces.
(set-face-attribute 'col-highlight nil :background "#555")
(set-face-attribute 'hl-line nil :background "#555")

;; Put customize-mode stuff into a separate ``local'' init file.
(setq custom-file (expand-file-name "~/.emacs.d/init-local.el"))
(load custom-file 'noerror)

;; Resize frames pixelwise.
(setq frame-resize-pixelwise t)
